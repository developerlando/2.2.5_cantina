### ARBOL DE INSTANCIAS###
## MENU PRINCIPAL
# 1 COMPRAR RESALTADO
# 2 INVENTARIO RESALTADO
# 3 REGISTROS RESALTADO
# 4 SALIR
## MENU COMPRAR
# 11 AGREGAR RESAlTADO
# 12 USUARIO RESALTADO
# 13 FACTURAR RESALTADO
# 14 VOLVER RESALTADO
## MENU INVENTARIO
# 21 AGREGAR ITEM RESALTADO
# 22 USUARIO RESALTADO
# 23 GUARDAR RESALTADO
# 24 VOLVER RESALTADO
## MENU REGISTROS
# 31 VER TODOS LOS REGISTROS
# 32 VERIFICAR ULTIMO REGISTRO
# 33 VER OBJETOS
# 34 VOLVER

### DATOS UTILES
bundle add faker (TE PERMITE LLENAR LA BASE DE DATOS)###
require 'faker'
name: Faker::Music.genre, 
description: Faker::Lorem.paragragh
def self.seeds funcion comun para llenado

### DATOS DE MIGRACION ###
rake db:new_migration name=CreateOwner options="username password"
rake db:new_migration name=CreateProduct options="name description price:decimal stock:numeric"
rake db:new_migration name=CreateClient options="name ci:numeric balance:decimal email" 
rake db:new_migration name=CreateSale options="date:date"
rake db:new_migration name=CreateProductSale options="quantity:numeric"
rake db:new_migration name=CreateCategory options="name"

rake db:new_migration name=CreateJoinTableProductCategory options="products categories"

rake db:new_migration name=AddRefOwnerToProduct options="owner:references"
rake db:new_migration name=AddRefClientToSale options="client:references"
rake db:new_migration name=AddRefSaleToProductSale options="sale:references"
rake db:new_migration name=AddRefProductToProductSale options="product:references"
rake db:new_migration name=AddPriceToProductSale options="price:decimal"


rake db:migrate

### PERMISOS ###
chmod +x bin/console.rb