
require 'pry'
require 'colorize'
require_relative '../db/connection'
require_relative '../models/category'
require_relative '../models/client'
require_relative '../models/owner'
require_relative '../models/product'
require_relative '../models/productsale'
require_relative '../models/sale'

