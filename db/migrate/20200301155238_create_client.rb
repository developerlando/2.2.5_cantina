class CreateClient < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.numeric :ci
      t.decimal :balance
      t.string :name
      t.string :email
    end
    add_index :clients, :ci, unique: true
  end
end
