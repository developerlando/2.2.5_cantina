class CreateSale < ActiveRecord::Migration[5.2]
  def change
    create_table :sales do |t|
      t.date :date
    end
  end
end
