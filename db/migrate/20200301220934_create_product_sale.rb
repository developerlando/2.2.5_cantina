class CreateProductSale < ActiveRecord::Migration[5.2]
  def change
    create_table :product_sales do |t|
      t.numeric :quantity
    end
  end
end
