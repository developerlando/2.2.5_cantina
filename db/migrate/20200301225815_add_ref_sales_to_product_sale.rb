class AddRefSalesToProductSale < ActiveRecord::Migration[5.2]
  def change
    add_reference :product_sales, :sale, foreign_key: true
  end
end
