class AddRefProductToProductSale < ActiveRecord::Migration[5.2]
  def change
    add_reference :product_sales, :product, foreign_key: true
  end
end
