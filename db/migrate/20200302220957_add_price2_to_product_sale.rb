class AddPrice2ToProductSale < ActiveRecord::Migration[5.2]
  def change
    add_column :product_sales, :price, :decimal
  end
end
