# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_02_220957) do

  create_table "categories", force: :cascade do |t|
    t.string "name"
  end

  create_table "categories_products", id: false, force: :cascade do |t|
    t.integer "product_id", null: false
    t.integer "category_id", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.decimal "ci"
    t.decimal "balance"
    t.string "name"
    t.string "email"
    t.index ["ci"], name: "index_clients_on_ci", unique: true
  end

  create_table "owners", force: :cascade do |t|
    t.string "username", null: false
    t.string "password"
  end

  create_table "product_sales", force: :cascade do |t|
    t.decimal "quantity"
    t.integer "sale_id"
    t.integer "product_id"
    t.decimal "price"
    t.index ["product_id"], name: "index_product_sales_on_product_id"
    t.index ["sale_id"], name: "index_product_sales_on_sale_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
    t.decimal "price", null: false
    t.decimal "stock", null: false
    t.integer "owner_id"
    t.index ["name"], name: "index_products_on_name", unique: true
    t.index ["owner_id"], name: "index_products_on_owner_id"
  end

  create_table "sales", force: :cascade do |t|
    t.date "date"
    t.integer "client_id"
    t.index ["client_id"], name: "index_sales_on_client_id"
  end

end
