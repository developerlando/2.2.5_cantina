require 'colorize'
require_relative 'mad'
require_relative 'reg'
require_relative '../bin/console.rb'

class App
	@@key = '0'
	@@status ={instance: 1, 
						 status: 0,
						 client: 0}
	def self.ini
		password = Owner.exist?
		while password
			puts ("Introduce Pass: ")
			user_pass = gets.chomp.to_s
			password = false if user_pass == Owner.pass || user_pass == '.'
		end
		loop do
			Screen.show(@@status)
			@@key = capture_input
			menu_process
			break if @@status[:instance] == 41 || @@key == 's'
		end
	end

	private
#ENTRADA DE DATOS
	def self.capture_input
		system("stty raw -echo")
		t = STDIN.getc
		system("stty -raw echo")
		return t
	end
#MENU_PROCESS
	def self.menu_process
	 	@@key.downcase!
	  case @@key.downcase.ord 
	  when 97..98 #ARRIBA - ABAJO
	    case @@status[:instance]
	    when 1..2
	    	@@status[:instance] = @@status[:instance] + 2
	    when 3..4
	    	@@status[:instance] = @@status[:instance] - 2
			when 11..12
				@@status[:instance] = @@status[:instance] + 2
	    when 13..14
				@@status[:instance] = @@status[:instance] - 2
			when 15..16
				@@status[:instance] = @@status[:instance] + 2
			when 17..18
				@@status[:instance] = @@status[:instance] - 2
	    when 21..22
	    	@@status[:instance] = @@status[:instance] + 2
	    when 23..24
	    	@@status[:instance] = @@status[:instance] - 2
	    when 31..32
	    	@@status[:instance] = @@status[:instance] + 2
	    when 33..34
	    	@@status[:instance] = @@status[:instance] - 2
	    end
    when 99..100
	    if @@status[:instance]%2 == 0
				@@status[:instance] -= 1 	
			else
				@@status[:instance] += 1
			end
		when 13# "ENTER"
			case @@status[:instance] 
			when 1..9
				@@status[:instance] = @@status[:instance]*10+1 if @@status[:instance] < 10
				@@status[:status] = 0
				@@status[:client] = 0
	    when 11
				@@status[:status]= Mad.new_item
			when 12
				@@status[:instance] = 15
			when 13 #PERSISTIR COMPRA
				case Mad.check
				when 0
					puts "FALTAN DATOS DEL CLIENTE".red
					print "presiona enter para continuar"
					gets.chomp
				when 1
					puts "INCONGRUENCIAS CON LOS PRODUCTOS".red
					print "presiona enter para continuar"
					gets.chomp
				when 2
					if Mad.persist 
						Mad.reset
						@@status = Mad.refresh
						@@status[:instance]= 32
						Mad.update(@@status)
					else
						puts "VERIFICAR BALANCE DEL CLIENTE".red
						print "presiona enter para continuar"
						gets.chomp
					end
				end
			when 15 #INTRODUCIR CEDULA
			Mad.new_client unless (Mad.client)
			@@status = Mad.refresh
			when 16 #MODIFICANDO DATA CLIENTE
				if @@status[:client] == 1
					Mad.new_client
					@@status[:instance] = 18
				else
					@@status[:instance] = 15
				end
			when 14 #VOLVER
				Mad.reset
				@@status = Mad.refresh
			when 17
				if @@status[:client] == 1
					Mad.persist_client
				end
				@@status[:instance]=18
			when 21
					@@status[:status] = Mad.new_product
					Mad.update(@@status)
			when 23
				if @@status[:status] == 1
					Mad.new_product_save
					Mad.reset
					@@status = Mad.refresh
				end
			when 31 ##
				@@status[:status] = 1
				Mad.update(@@status)
				Reg.menu
			when 32
				Mad.reset
				@@status[:status]= Mad.last
				Mad.update(@@status)
			when 33
				Mad.reset
				Mad.inventary
			when 18
				@@status[:instance] = 11
	    when 24 #VOLVER
				Mad.reset
				@@status =Mad.refresh
	    when 34 #VOLVER
	    	Mad.reset
				@@status =Mad.refresh
	    end
  	end	
	end
end

