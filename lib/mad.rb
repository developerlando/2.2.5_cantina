require_relative 'app'
require_relative 'screen'
require_relative '../bin/console.rb'

class Mad
	SS = 80 ## ESPACIO DE PANTALLA
	SL = 15 ## ESPACIO DE PANTALLA LARGO
	@@data = []
	@@status = {instance:    0   ,
							status: 0         ,
							line: 0,
							client: 0 }
	@@client_temp = {ci: 0,name: 'default', balance:0 , email: '5@gmail.com'}
	@@tprice = 0
## MUESTRA LA PANTALLA DE DATOS	
	def self.show(value)
		@@status[:instance] = value[:instance]
		case @@status[:instance]
		when 1..4
			Screen.margin_top
			(SL).times do
				Screen.margin_rigth
				(SS).times{print ' '}
				Screen.margin_left
			end
		when 11..14
			if @@status[:status] >= 1
				title_sale
				show_data
				surcharge
				line_tprice
			else
				screen_white
			end
		when 15..18
			title_client
			show_data_client
			surcharge_client
		when 21..24
			if @@status[:status] >= 1
				title_product
				show_data_product
				surcharge
			else
				screen_white
			end
		when 31..34
			if @@status[:line] == 1
				title_product
				show_data_product
			else
				if @@status[:status] >= 1
					title_last
					show_data_last
					line_tprice
				else
					show_stadist
				end
			end
		end
	end
#AGREGAR DATA
	def self.new_item
		temp = {name: 's' , quantity: 0 , price: 0}
		loop do
			puts 'Indica el Nombre del Producto'
			temp[:name] = gets.chomp.to_s
			return 0 if temp[:name] == '.'
	  break if Product.exist?(temp)		
		end
		product_data = Product.find_by(name: temp[:name])
		loop do
			puts "Cantidad, Quedan #{"#{product_data[:stock]}".yellow}"
			temp[:quantity] = gets.chomp.to_i
			return 0 if temp[:quantity] == 0
		break if  temp[:quantity] == 0 || Product.enough?(temp, @@data)
		end
			temp[:price] = Product.price * temp[:quantity] 
	  	@@data << temp
			@@status[:status] += 1
			@@tprice = Product.count(@@data)
		@@status[:status]
	end
#NEW PRODUTO
	def self.new_product
		temp = {name: 's' , stock: 0 , price: 0, description: 's', category: 's'}
		puts 'Indica el Nombre del Producto'
		temp[:name] = gets.chomp.to_s
		temp[:name].downcase!
		exist = Product.revise(temp)
		puts "Cuantos #{"#{exist[:name]}".yellow} deseas #{"AGREGAR".red}, actual #{"#{exist[:stock]}".yellow}"
		exist[:stock] = gets.chomp.to_i
		puts 'Descripcion del producto'
		exist[:description] = gets.chomp.to_s
		return 0 if exist[:description] == '.'
		puts "A cuanto se venderan los #{"#{exist[:name]}".yellow}, actual #{"#{exist[:price]}".yellow}"
		exist[:price] = gets.chomp.to_i
		puts "A que categoria pertenecen los #{"#{exist[:name]}".yellow}"
		temp[:category] = gets.chomp.to_s
		@@data << temp
		1
	end	
#GUARDAR PRODUCTO
	def self.new_product_save
		@@data.each do |product|
			p @@data
			temp = Product.find_by(name: product[:name])
			product[:category].downcase!
			category = Category.find_or_create_by(name: product[:category])
			if temp.nil?
				begin
				item = Product.create(name: product[:name] ,stock: product[:stock] ,price: product[:price] ,description: product[:description] )
				item.categories << category
				rescue=> exception
					puts 'NOMBRE YA EXISTE'.red
				end
			else
				begin
				Product.update(name: product[:name] ,stock: temp[:stock] + product[:stock] , price: product[:price] ,description: product[:description])
				item = Product.find_by(name: product[:name])
				item.categories << category
				rescue => exception
					puts 'NOMBRE YA EXISTE'.red
				end
			end
		end
	end
#PANTALLA SIN DATA	
	def self.screen_white
		Screen.margin_top
		(SL).times do
			Screen.margin_rigth
			(SS).times{print ' '}
			Screen.margin_left
		end
	end	
#TITULO DE PRODUCTOS
	def self.title_last
		Screen.margin_top
		Screen.margin_rigth
		f1 = "ULTIMA TRANSACCION"
		print f1.red
		sa = f1.length
		(SS-sa).times{print ' '}
		Screen.margin_left
	end
#TITULO DE CLIENTES
	def self.title_client
		Screen.margin_top
		Screen.margin_rigth
			print "Nombre: "
			(25 - ("Nombre: ").length).times{print ' '}
			print "Email: "
			(25 - ("Email: ").size).times{print ' '}
			print "CI: "
			(15 -  ("CI: ").size).times{print ' '}
			print "balance: "
			(15 -  ("balance: ").size).times{print ' '}
			Screen.margin_left
	end
##TITULO DE VENTAS
	def self.title_sale
		Screen.margin_top
		Screen.margin_rigth
		print "Nombre Del producto"
		(40 - ("Nombre Del producto").length).times{print ' '}
		print "| "
		print "Cantidad"
		(20 - 2- ("Cantidad").size).times{print ' '}
		print "| "
		print "Precio"
		(20- 2 - ("Precio").size).times{print ' '}
		Screen.margin_left
	end
##TITULO DE ITEM		
	def self.title_item
		Screen.margin_top
		Screen.margin_rigth
		print "Nombre Del producto"
		(40 - ("Nombre Del producto").length).times{print ' '}
		print "| "
		print "Cantidad"
		(20 - 2- ("Cantidad").size).times{print ' '}
		print "| "
		print "Precio"
		(20- 2 - ("Precio").size).times{print ' '}
		Screen.margin_left
	end
## TITULO ESTADISTICAS
	def self.show_stadist
		Screen.margin_top
		line_best_client
		line_best_products
		line_worse_products
	end
## TITULO PRODUCTO
	def self.title_product
		Screen.margin_top
		Screen.margin_rigth
		print "Nombre Del producto"
		(30 - ("Nombre Del producto").length).times{print ' '}
		print "| "
		print "Stock"
		(10 - 2- ("Stock").size).times{print ' '}
		print "| "
		print "Precio"
		(15- 2 - ("Precio").size).times{print ' '}
		print "| "
		print "Descripcion"
		(25- 2 - ("Descripcion").size).times{print ' '}
		Screen.margin_left
	end
#MUESTRA LA DATA DEL ITEM	
	def self.show_data
		@@data.each do |element|
			Screen.margin_rigth
			print "#{element[:name]}"
			(40 - ("#{element[:name]}").length).times{print ' '}
			print "| "
			print "#{element[:quantity]}"
			(20 - 2- ("#{element[:quantity]}").size).times{print ' '}
			print "| "
			print "#{element[:price]}"
			(20- 2 - ("#{element[:price]}").size).times{print ' '}
			Screen.margin_left
		end
	end
	def self.show_data_last
		@@data.each_with_index do |element, index|
			Screen.margin_rigth
			if index == 0
				f1 = "#{element[:name]} "
				f2 = "ci: "
				f3 = "#{element[:ci]}"
				sa = f1.length + f2.length + f3.length
				print f1
				print f2
				print f3
				(SS - sa).times{print ' '}
				Screen.margin_left
				Screen.margin_rigth
				f1 = "Nombre Del producto"
				print f1.red
				(40 - f1.length).times{print ' '}
				f2 = "| Stock"
				print f2.red
				(20 - f2.size).times{print ' '}
				f3 = "| Precio"
				print f3.red
				(20- f3.size).times{print ' '}
				Screen.margin_left
			else 
				f1 = "#{element[:name]}"
				print f1
				(40 - f1.length).times{print ' '}
				f2 = "| #{element[:quantity]}"
				print f2
				(20 - f2.size).times{print ' '}
				f3 = "| #{element[:price]}"
				print f3
				(20- f3.size).times{print ' '}
				Screen.margin_left
			end
		end
	end
#MUESTRA LA DATA DE PRODUCT
def self.show_data_product
	@@data.each do |element|
		Screen.margin_rigth
		print "#{element[:name]}"
		(30 - ("#{element[:name]}").length).times{print ' '}
		print "| "
		print "#{element[:stock]}"
		(10 - 2- ("#{element[:stock]}").size).times{print ' '}
		print "| "
		print "#{element[:price]}"
		(15- 2 - ("#{element[:price]}").size).times{print ' '}
		print "| "
		print "FINO!"
		(25- 2 - ("FINO!").size).times{print ' '}
		Screen.margin_left
	end
end
#MUESTRA LA DATA DEL CLIENTE
	def self.show_data_client
		if @@status[:client] == 1
			Screen.margin_rigth
			print "#{@@client_temp[:name]}"
			(25 - ("#{@@client_temp[:name]}").length).times{print ' '}
			print "#{@@client_temp[:email]}"
			(25 -  ("#{@@client_temp[:email]}").size).times{print ' '}
			print "#{@@client_temp[:ci]}"
			(15 - ("#{@@client_temp[:ci]}").size).times{print ' '}
			print "#{@@client_temp[:balance]}"
			(15 - ("#{@@client_temp[:balance]}").size).times{print ' '}
			Screen.margin_left
		else
			Screen.margin_rigth
			(SS).times{print ' '}
			Screen.margin_left
		end
	end
#RELLENA LA PANTALLA ITEM
	def self.surcharge
		if SL-1-@@data.size > 0
			(SL-1-@@data.size).times do
				Screen.margin_rigth
				(SS).times{print ' '}
				Screen.margin_left
			end
		end
	end
#RELLENA LA PANTALLA CLIENTE
	def self.surcharge_client
			(SL-2).times do
				Screen.margin_rigth
				(SS).times{print ' '}
				Screen.margin_left
		end
	end
### LINEAS SEPARADAS
##TPRECIO
	def self.line_tprice
		if @@tprice > 0
		Screen.margin_rigth
		print 'TOTAL COSTO DE ARTICULOS'.red
		(SS-10-('TOTAL COSTO DE ARTICULOS').length-("#{@@tprice}").size).times{print ' '}
		print "#{@@tprice}".yellow
		(10).times{print ' '}
		Screen.margin_left
		end
	end
	def self.line_best_client
		Screen.margin_rigth
		temp = Sale.best_client
		f1 = 'MEJOR CLIENTE: '
		f2 = "#{temp[:name]} "
		f3 = "ci: "
		f4 = "#{temp[:ci]} "
		f5 = "email: "
		f6 = "#{temp[:email]} "
		print f1.red
		print f2.yellow
		print f3.red
		print f4.yellow
		print f5.red
		print f6.yellow
		sa = f1.length + f2.length + f3.length + f4.length + f5.length + f6.length
		(SS-sa).times{print ' '}
		Screen.margin_left
	end
##MEJOR PRODUCTO
	def self.line_best_products
		Screen.margin_rigth
		temp = ProductSale.best_products
		f1 = 'PRODUCTO MAS HOT: '
		f2 = "#{temp[:name]} "
		f3 = "Precio: "
		f4 = "#{temp[:price]} "
		f5 = "Quedan "
		f6 = "#{temp[:stock]} "
		print f1.red
		print f2.yellow
		print f3.red
		print f4.yellow
		print f5.red
		print f6.yellow
		sa = f1.length + f2.length + f3.length + f4.length + f5.length + f6.length
		(SS-sa).times{print ' '}
		Screen.margin_left
	end
##PEOR PRODUCTO
	def self.line_worse_products
		Screen.margin_rigth
		temp = ProductSale.worse_products
		f1 = 'PRODUCTO MAS BAD: '
		f2 = "#{temp[:name]} "
		f3 = "Precio: "
		f4 = "#{temp[:price]} "
		f5 = "Quedan "
		f6 = "#{temp[:stock]} "
		print f1.red
		print f2.yellow
		print f3.red
		print f4.yellow
		print f5.red
		print f6.yellow
		sa = f1.length + f2.length + f3.length + f4.length + f5.length + f6.length
		(SS-sa).times{print ' '}
		Screen.margin_left
	end
#CLIENTES 
##INTRODUCIR CEDULA
	def self.client
		@@client_temp = {ci: 0,name: 'default', balance:0 , email: '5@gmail.com'}
		puts 'Introduce la ci:(formato sin puntos o comas'
		@@client_temp[:ci] = gets.chomp.to_i
		if Client.exist?(@@client_temp)
			@@client_temp[:name] = Client.rename
			@@client_temp[:balance] = Client.balance
			@@client_temp[:email] = Client.email
			@@status[:client] = 1
		end
		Client.exist?(@@client_temp)
	end
#NUEVO CLIENTE o MODIFICAR CLIENTE
	def self.new_client
		puts 'Indicame Nombre del Cliente'
		@@client_temp[:name] = gets.chomp.to_s
		puts "Indica El monto a agregar (actual: #{@@client_temp[:balance]})"
		up = gets.chomp.to_f
		if up >= 1000
			@@client_temp[:balance] += up+up*20/100
		else
			@@client_temp[:balance] -= up
		end
		puts 'Indicame una direccion de correo'
		@@client_temp[:email] = gets.chomp.to_s
		@@status[:client] = 1
	end
#RESETEA LOS STATUS
	def self.reset
		@@data = []
		@@tprice = 0
		@@client_temp = {ci: 0,name: 'default', balance:0 , email: '5@gmail.com'}
		@@status = {instance:    1   ,
								status: 0         ,
								line: 0,
								client: 0 }
	end
#VERIFICA QUE LA COMPRA CUMPLA CON TODOS LOS REQUISITOS
	def self.check
		if @@status[:client] == 1
			if @@status[:status] >= 1
				return 2
			else
				return 1
			end
		else
			return 0
		end
	end
#COLOCA LAS COSAS AL DIA MAD A APP
	def self.refresh
	@@status
	end
#PERSISTE LA DATA WARNING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	def self.persist
		if @@client_temp[:balance] > @@tprice
			Product.index(@@data)
			@@tprice = Product.count(@@data)
			@@client_temp[:balance]-= @@tprice
			Client.index(@@client_temp)
			Sale.index(@@client_temp)
			ProductSale.index(@@data)
			true
		else
			false
		end
	end
	
	def self.persist_client
		Client.index(@@client_temp)
	end
# COSAS AL DIA DE APP A MAD
	def self.update(status_app)
	 @@status = status_app
	end
#TRAE LA ULTIMA VENTA
	def self.last
		last_client = Client.latest
		@@data << last_client
		temp_quantity = ProductSale.latest
		temp_products = Product.latest(temp_quantity)
		temp_quantity.each_with_index do |element, index|
			temp = 	{ quantity: element[:quantity],
				name: temp_products[index][:name],
				price: element[:quantity]*element[:price]
							}
			@@data << temp
		end
		@@tprice = count_last
		@@status[:status] = 1
	end
#TPRICE DE ULTIMA COMPRA
	def self.count_last
		count = 0
		for i in 1...@@data.size
			count+=@@data[i][:price]
		end
		count
	end
#VER INVENTARIO
	def self.inventary
		begin			
		@@data = Product.find_by_sql "select * from products where stock > 0"
		@@status[:line] = 1
		rescue => exception
		end
	end
end