require_relative '../bin/console'

class Reg
  def self.menu
    system('clear')
    system('clear')
    loop do 
      puts 'Sobre que deseas buscar - Disponibles: clientes, productos, ventas, s. para salir'
      decision = gets.chomp.to_s
      decision.downcase!
      case decision 
      when 'clientes'
        loop do
          s1 = 'indica con un numero que deseas: 1. Compras por cliente, 0.para salir'
          puts s1
          decision2 = gets.chomp.to_i
          case decision2
          when 1
            buy_for_client
          when 2
          when 0
            return
          end
        end
      when 'productos'
        loop do
          s2 = 'indica con un numero que deseas: 1. Productos por Categoria, 0.para salir'
          puts s2
          decision2 = gets.chomp.to_i
          case decision2
          when 1
            product_for_categories
          when 2
          when 0
            return
          end
        end
      when 'ventas'
        loop do
          f1 = 'indica con un numero que deseas: 1. Ventas por Fecha, 0.para salir'
          puts f1
          decision2 = gets.chomp.to_i
          case decision2
          when 1
            sell_for_date
          when 2
          when 0
            return
          end
        end
      when 's'
        return 
      else 
        puts "Verifica que escribiste correctamente".red
      end
    end
  end
private
#VENTAS POR FECHA
  def self.sell_for_date
    s4 = "INDICA LA FECHA INICIAL".red
    s5 = "(dd) dia: ".blue
    s6 = "(mm) mes: ".blue
    s7 = "(yyyy) a-o: ".blue 
    s8 = "INDICA LA FECHA FINAL".red
    begin
      puts s4
      print s5 #DIA 
      day_i = gets.chomp.to_s
      print s6 #MES
      mes_i = gets.chomp.to_s
      print s7 #ANO
      ao_i = gets.chomp.to_s
      puts s8
      print s5 #DIA
      day_f = gets.chomp.to_s
      print s6 #MES
      mes_f = gets.chomp.to_s
      print s7 #ANO
      ao_f = gets.chomp.to_s
      temp_sale =Sale.find_by_sql "SELECT * FROM sales WHERE date BETWEEN '#{ao_i}-#{mes_i}-#{day_i} 00:00' AND '#{ao_f}-#{mes_f}-#{day_f} 23:59'"
      temp_sale.each do |sale|
        client = Client.find_by(id: sale[:client_id])
        product_list = ProductSale.all(sale)
        print "Comprador: ".red 
        print "#{client[:name].capitalize} ".yellow
        print "ci: ".red
        print "#{client[:ci]} ".yellow
        print "Fecha: ".red
        print "#{sale[:date]}".yellow
        puts
        count = 0
        product_list.each do |product|
        data_product = Product.find_by(id: product[:product_id])
          print "Producto: ".blue
          print " #{data_product[:name]} ".yellow
          print "Cantidad: ".blue
          print " #{product[:quantity]} ".yellow
          print "Precio: ".blue
          print " #{product[:quantity]*product[:price]}".yellow
          count += product[:quantity]*product[:price]
          puts
        end 
        print "Total Pagado: ".red
        print "#{count}".yellow
        puts
        print "----------------------------------------------".blue
        puts
      end
    rescue => exception
      puts "FECHA MAL INGRESADA".red
    end
    print "PULSA UNA TECLA PARA CONTINUAR".red
    key= gets.chomp
  end
#COMPRAS POR CLIENTE
  def self.buy_for_client

    s1 = "Indica la ci del cliente, s. salir"
    s2 = "Cliente no Encontrado"
    client = 0
    
    loop do ##CLIENTE POR NOMBRE
      puts s1
      ci_client = gets.chomp.to_s
      client = Client.find_by(ci: ci_client)
      break if ci_client == 's' || !(client.nil?)
      puts s2.red
    end
    temp_sale =Sale.find_by_sql "SELECT * FROM sales WHERE client_id = '#{client[:id]}'"
    count_global = 0
    temp_sale.each do |sale|
      product_list = ProductSale.all(sale)
      print "Comprador: ".red 
      print "#{client[:name].capitalize} ".yellow
      print "ci: ".red
      print "#{client[:ci]} ".yellow
      print "Fecha: ".red
      print "#{sale[:date]}".yellow
      puts
      count = 0
      product_list.each do |product|
        data_product = Product.find_by(id: product[:product_id])
        print "Producto: ".blue
        print " #{data_product[:name]} ".yellow
        print "Cantidad: ".blue
        print " #{product[:quantity]} ".yellow
        print "Precio: ".blue
        print " #{product[:quantity]*product[:price]}".yellow
        count += product[:quantity]*product[:price]
        puts
      end 
      print "Total Pagado: ".red
      print "#{count}".yellow
      puts
      print "----------------------------------------------".blue
      puts
      count_global += count
    end
    puts "#{"#{client[:name]}".yellow} a gastado #{"#{count_global}".yellow} "
    puts
    print "Presiona una tecla para continuar"
    key = gets.chomp
    system('clear')
    system('clear')
  end

##PRODUCTOS LISTADOS POR CATEGORIA
  def self.product_for_categories
    s1 = "Indica la categoria que deseas buscar: disponibles( "
    categories = Category.find_by_sql "SELECT * FROM categories"
    categories.each do |category|
      s1 +="#{category[:name]} ".yellow
    end
    s1 += ") o ingresa all para verificar toda la lista"
    print s1
    puts
    name_category = gets.chomp.to_s
    puts
    begin    
    if name_category != 'all'
      category = Category.find_by(name: name_category)
      print "CATEGORIA: ".red
      print "#{category[:name]}".yellow
      puts
      print "Productos(#{category.products.length}): ".red
      puts
      category.products.map do |product|
        print "Nombre: ".blue
        print"#{product[:name]} ".yellow
        print "Cantidad: ".blue
        print "#{product[:stock]} ".yellow
        print "Precio: ".blue
        print "#{product[:price]} ".yellow
        print "Descripcion: ".blue
        print "#{product[:description]}".yellow
        puts
      end 
    else
      list_category = Category.find_by_sql "SELECT * FROM categories"
      list_category.each do |category|
        print "CATEGORIA: ".red
        print "#{category[:name]}".yellow
        puts
        print "Productos(#{category.products.length}): ".red
        puts
        category.products.map do |product|
          print "Nombre: ".blue
          print"#{product[:name]} ".yellow
          print "Cantidad: ".blue
          print "#{product[:stock]} ".yellow
          print "Precio: ".blue
          print "#{product[:price]} ".yellow
          print "Descripcion: ".blue
          print "#{product[:description]}".yellow
          puts
        end 
      end
    end
    puts
    print "Presiona una tecla para continuar"
    key = gets.chomp
    system('clear')
    system('clear')
    rescue => exception
      system('clear')
      system('clear')
      puts 'CATEGORIA NO ENCONTRADA'.red
    end
  end
end