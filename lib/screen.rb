require_relative 'app'
class Screen
	SS = 80 ## ESPACIO DE PANTALLA
	SL = 15 ## ESPACIO DE PANTALLA LARGO
	def self.show(value)
		@@status = value
		system('clear')
		system('clear')
		case @@status[:instance]
		when 1..4 #PRINCIPAL
			show_instance_one
		when 11..14 #COMPRAR
			show_instance_eleven
		when 15..18 #Cliente
			show_instance_client
		when 21..24 #INVENTARIO
			show_instance_twenty_one
		when 31..34 #REGISTROS
			show_instance_thrirty_one
		end
	end
#PANTALLAS
#MOSTRAR PANTALLA
	#PANTALLA COMPRAR RESALTADO
	def self.show_instance_one
		value1 = ('c. Comprar').length + 'i. Inventario'.length
		value2 = ('r. Ver Registros').length + ('s. Salir').length
		Mad.show(@@status)
		margin_top
		margin_rigth
		print 'c. Comprar'.yellow if @@status[:instance] == 1
		print 'c. Comprar' unless @@status[:instance] == 1
		(SS-value1).times{print ' '}
		print 'i. Inventario'.yellow if @@status[:instance] == 2
		print 'i. Inventario' unless @@status[:instance] == 2
		margin_left
		margin_rigth
		print 'r. Ver Registros'.yellow if @@status[:instance] == 3
		print 'r. Ver Registros' unless @@status[:instance] == 3
		(SS-value2).times{print ' '}
		print 's. Salir'.yellow if @@status[:instance] == 4
		print 's. Salir' unless @@status[:instance] == 4
		margin_left
		margin_top
	end
	#PANTALLA COMPRA
	def self.show_instance_eleven
		value1 = 'f. Agregar Item'.length + 'c. Datos del Cliente'.length
		value2 = 'r. Vender'.length + 'v. Volver'.length if @@status[:status] >= 1
		value2 = 'v. Volver'.length unless @@status[:status] >= 1
		Mad.show(@@status)
		margin_top
		margin_rigth
		print 'f. Agregar Item'.yellow if @@status[:instance] == 11
		print 'f. Agregar Item' unless @@status[:instance] == 11
		(SS-value1).times{print ' '}
		print 'c. Datos del Cliente'.yellow if @@status[:instance] == 12
		print 'c. Datos del Cliente' unless @@status[:instance] == 12
		margin_left
		margin_rigth
		print 'r. Vender'.yellow if @@status[:instance] == 13 && @@status[:status] >= 1
		print 'r. Vender' if @@status[:status] >= 1 && @@status[:instance] != 13
		(SS-value2).times{print ' '}
		print 'v. Volver'.yellow if @@status[:instance] == 14
		print 'v. Volver' unless @@status[:instance] == 14
		margin_left
		margin_top
	end
	#PANTALLA INVENTARIO
	def self.show_instance_twenty_one
		value1 = 'f. Agregar Producto'.length + 'c. Owner'.length
		value2 = 'v. Volver'.length unless @@status[:status] >= 1
		value2 = 'g. Guardar'.length + 'v. Volver'.length if @@status[:status] >= 1
		Mad.show(@@status)
		margin_top
		margin_rigth
		print 'f. Agregar Producto'.yellow if @@status[:instance] == 21
		print 'f. Agregar Producto'  unless @@status[:instance] == 21
		(SS-value1).times{print ' '}
		print 'c. Owner'.yellow if @@status[:instance] == 22
		print 'c. Owner' unless @@status[:instance] == 22
		margin_left
		margin_rigth
		print 'g. Guardar'.yellow if @@status[:instance] == 23 && @@status[:status] >= 1
		print 'g. Guardar' if @@status[:instance] != 23 && @@status[:status] >= 1
		(SS-value2).times{print ' '}
		print 'v. Volver'.yellow if @@status[:instance] == 24
		print 'v. Volver' unless @@status[:instance] == 24
		margin_left
		margin_top
	end
#PANTALLA REGISTROS
	def self.show_instance_thrirty_one
		value1 = 'a. Ver Todas las Transacciones'.length + 'u. Ultima Transaccion'.length
		value2 = 'o. Ver Objetos Disponibles'.length + 'v. Volver'.length
		Mad.show(@@status)
		margin_top
		margin_rigth
		print 'a. Ver Todas las Transacciones'.yellow if @@status[:instance] == 31
		print 'a. Ver Todas las Transacciones'  unless @@status[:instance] == 31
		(SS-value1).times{print ' '}
		print 'u. Ultima Transaccion'.yellow if @@status[:instance] == 32
		print 'u. Ultima Transaccion' unless @@status[:instance] == 32
		margin_left
		margin_rigth
		print 'o. Ver Objetos Disponibles'.yellow if @@status[:instance] == 33
		print 'o. Ver Objetos Disponibles' unless @@status[:instance] == 33
		(SS-value2).times{print ' '}
		print 'v. Volver'.yellow if @@status[:instance] == 34
		print 'v. Volver' unless @@status[:instance] == 34
		margin_left
		margin_top
	end
#CLIENTES
	def self.show_instance_client
		value1 = 'c. Introducir la Cedula'.length + 'm. Modificar Data'.length if @@status[:client] >= 1
		value1 = 'c. Introducir la Cedula'.length unless @@status[:client] >= 1
		value2 = 'g. Guardar'.length + 'v. Volver'.length if @@status[:client] >= 1
		value2 = 'v. Volver'.length unless @@status[:client] >= 1
		Mad.show(@@status)
		margin_top
		margin_rigth
		print 'c. Introducir la Cedula'.yellow if @@status[:instance] == 15
		print 'c. Introducir la Cedula'  unless @@status[:instance] == 15
		(SS-value1).times{print ' '}
		print 'm. Modificar Data'.yellow if @@status[:instance] == 16 && @@status[:client] == 1
		print 'm. Modificar Data' if @@status[:instance] != 16 && @@status[:client] == 1
		margin_left
		margin_rigth
		print 'o. Guardar'.yellow if @@status[:client] == 1 && @@status[:instance] == 17
		print 'o. Guardar' if @@status[:client] == 1 && @@status[:instance] !=  17
		(SS-value2).times{print ' '}
		print 'v. Volver'.yellow if @@status[:instance] == 18
		print 'v. Volver' unless @@status[:instance] == 18
		margin_left
		margin_top
	end
#MARGENENES 
	#MARGEN SUPERIOR
	def self.margin_top
		(SS+4).times{print " ".colorize(:background => :white)}
		puts
	end
	#MARGEN DERECHO
	def self.margin_rigth
		print "  ".colorize(:background => :white) 
	end
	#MARGEN IZQUIERDO
	def self.margin_left
		margin_rigth
		puts
	end
end