class Client < ActiveRecord::Base #UN CLIENTE POSEE VARIAS COMPRAS
  has_many :sales

  def self.exist?(element)
    begin 
     @@client = Client.find_by(ci: element[:ci])
     if (@@client[:ci]).nil?
       false
     else
       true
     end
    rescue => exception
     false
    end
  end

   def self.rename
     @@client[:name]
   end
   def self.email
     @@client[:email]
   end
   def self.balance
     @@client[:balance]
   end
##ACTUALIZA CON LAS MODIFICACIONES
  def self.refresh(element)
    @@client[:name] = element[:name] 
    @@client[:email] = element[:email]
    @@client[:balance] += element[:balance]
  end
### PERSISTIR DATA
  def self.index(element)
    begin
      Client.create(ci: element[:ci], balance: element[:balance],name:element[:name],email:element[:email])
    rescue
      client = Client.find_by(ci: element[:ci])  
      client.update( balance: element[:balance],name:element[:name],email:element[:email])
    end
  end

  def self.latest
    temp_sale = Sale.last
    Client.find_by(id: temp_sale[:client_id])
  end
end