class Product < ActiveRecord::Base
belongs_to :owner #UN DUENO
has_and_belongs_to_many :categories
has_many :product_sales
has_many :sales, through: :product_sales
#EXISTE EL PRODUCTO?
	def self.exist?(element)
		 begin
			element[:name].downcase!
			@@product = Product.find_by(name: element[:name]) 
			if @@product[:id].nil?
				p 'no hay producto con dicho nombre'
				false
			elsif @@product[:stock] == 0
				p "Se acabaron los #{@@product[:name]}"
				false
			else
				true
			end
		 rescue => exception
			p 'no hay producto con dicho nombre'
			false
		 end
	end
#HAY SUFICIENTES
	def self.enough?(element, sale_list)
		to_sell = element[:quantity]
		sale_list.each {|item| to_sell += item[:quantity] if item[:name] == element[:name]}
		sold_out = to_sell - element[:quantity]
		if to_sell > @@product[:stock]
			puts "No puedes vender tantos productos solo quedan #{@@product[:stock] - sold_out}"
			false
		else
			true
		end
	end
#PRECIO DEL PRODUCTO
	def self.price
		@@product[:price]
	end
##PRECIO TOTAL
	def self.count(products)
		count = 0
		products.each do |product|
			temp = Product.find_by(name: product[:name])
			count += temp[:price]*product[:quantity]
		end
		count
	end
#PERSISTE LA DATA
	def self.index(products)
		products.each do |item|
			temp=Product.find_by(name: item[:name])
			temp.update(stock: temp[:stock] - item[:quantity])
		end
	end
#REVISA SI EXISTE UN PRODUCTO Y TRAE SUS DATOS
	def self.revise(product)
		begin
			item = Product.find_by(name: product[:name])
			if item[:id].nil?
				product
			else
				item
			end
		rescue => exception
			product
		end
	end
	def self.latest(products_array)
		data = []
		products_array.each do |product|
		temp =	Product.find_by(id: product[:product_id])
			data << temp
		end
		data
	end
end