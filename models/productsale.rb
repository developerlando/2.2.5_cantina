class ProductSale< ActiveRecord::Base
belongs_to :sale
belongs_to :product
  def self.index(products)
    products.each do |element|
      item = Product.find_by(name: element[:name])
      if element[:quantity] > 0
        temp = ProductSale.create(quantity: element[:quantity], product_id: item[:id], price: element[:price])
        temp.sale = relations
        temp.save
      end
    end
  end
  def self.relations
    Sale.last
  end
  def self.best_products
    begin
      group = ProductSale.find_by_sql "select product_id, count(product_id) from product_sales group by product_id order by count(product_id) desc "
      product = Product.find(group[0][:product_id])
    rescue => exception
      "un quilombo"
    end
  end
  def self.worse_products
    begin
      group = ProductSale.find_by_sql "select product_id, count(product_id) from product_sales group by product_id order by count(product_id) asc "
      product = Product.find(group[0][:product_id])
    rescue => exception
      "un quilombo"
    end
  end
  def self.latest
    begin
      temp_sale = Sale.last
      ProductSale.find_by_sql "select * from product_sales where sale_id = #{temp_sale[:id]}"
    rescue => exception
    end
  end
  def self.all(sale)
    ProductSale.find_by_sql "select * from product_sales where sale_id = #{sale[:id]}"
  end
end

  

  
